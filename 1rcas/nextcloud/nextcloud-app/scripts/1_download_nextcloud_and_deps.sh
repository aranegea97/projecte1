#!/bin/bash

# Install dependencies
apt-get update
apt-get install -y apache2 mariadb-server libapache2-mod-php
apt-get install -y php-gd php-json php-mysql php-curl php-mbstring
apt-get install -y php-intl php-imagick php-xml php-zip

# Add the repos containing the newer php packages
sudo apt install software-properties-common
sudo add-apt-repository ppa:ondrej/php
sudo apt -y update

# Install PHP 7.4 and extensions
sudo apt -y install php7.4 php7.4-{mysql,mbstring,bcmath,bz2,curl,imagick,dom,zip} libapache2-mod-security2 wget
sudo apt-get -y install php7.4-gd
#apache
sudo a2dismod php7.2
sudo a2enmod php7.4
# Restart the apache 2 service to activate the new configuration
sudo systemctl restart apache2




# Download and unpack Nextcloud
wget https://download.nextcloud.com/server/releases/latest.tar.bz2
tar -xjf latest.tar.bz2
