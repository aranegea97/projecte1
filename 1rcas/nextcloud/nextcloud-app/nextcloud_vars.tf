variable "admin_user" {
    description =  "Nextcloud admin user"
    default = "admin"
}

variable "admin_pass" {
    description =  "Nextcloud admin pass"
    default = "aran01"
}

variable "data_dir" {
    description =  "Data folder for Nextcloud"
    default = "/var/www/nextcloud/data"
}
