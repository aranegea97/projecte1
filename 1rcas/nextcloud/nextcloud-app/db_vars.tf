variable "db_name" {
    description = "Database name"
    default = "arandb"
}

variable "db_user" {
    description = "Database user"
    default = "aran"
}

variable "db_pass" {
    description = "Database password"
    default = "aran01"
}

variable "db_endpoint" {
    description = "Database endpoint url"
}
